use std::collections::HashMap;
use std::fs::{self, OpenOptions};
use std::path::{Path, PathBuf};
use std::result::Result;

use crate::num_format::ToFormattedString;

use super::{AppError, CreateModeArgs, multithread, singlethread, utils, watchdog};
use super::AppResult;
use super::discovery;
use super::lava_torrent::bencode::*;

type TorrentSection = HashMap<String, BencodeElem>;

struct SizeInfo {
    piece_size: u64,
    whole_size: u64
}

struct FileLists {
    file_list: Vec<String>,
    hash_list: Vec<String>,
}

pub fn run_create(m: CreateModeArgs) -> Result<(), AppError> {
    let locale = utils::default_locale();
    let mut file_list: Vec<String> = Vec::new();

    remove_output_if_needed(&m)?;

    if let Ok(_) = fs::metadata(&m.out_name) {
        error!("Name '{}' is taken by another file, try a different name.", m.out_name);
        return Err(AppError::NameIsBusy(m.out_name.clone()));
    }

    trace!("Discovering files...");

    let mut whole_size = 0u64;
    let base_dir = m.base_dir.as_ref();
    let empty_str = String::new();

    for e in m.paths.iter() {
        let mut prefixed_path = PathBuf::new();
        prefixed_path.push(base_dir.unwrap_or(&empty_str));
        prefixed_path.push(e);

        let prefixed_path_str = prefixed_path.to_str().unwrap().to_string();
        trace!("- Processing path: '{}'", prefixed_path_str);
        whole_size += discovery::discover_files(&prefixed_path_str, &m.excludes, &mut file_list)?;
    }

    info!("Discovered {} files.", file_list.len());

    if file_list.is_empty() {
        return Err(AppError::MissingFiles);
    }

    let piece_size = if m.piece_size.is_none() || m.piece_size.unwrap() == 0 {
        use std::f64::consts;
        use core::cmp;

        let mut exponent = (((whole_size as f64).log(consts::E) / consts::LN_2) - 10.0) as i64;
        exponent = cmp::min(cmp::max(15, exponent), 24);
        let piece_size = 2u64.pow(exponent as u32);

        trace!("Will generate {} pieces.", 1 + (whole_size / piece_size));

        piece_size
    } else {
        m.piece_size.unwrap()
    };

    trace!("Using piece size of {} bytes.", piece_size.to_formatted_string(&locale));

    if piece_size < 1 * 1024 || piece_size > 16 * 1024 * 1024 {
        error!("Error: piece size is outside the spec. Can't use this value.");
        return Err(AppError::WrongArguments);
    }

    if m.mt {
        trace!("Using multithreaded processing");
    } else {
        trace!("Using singlethreaded processing");
    }

    let mut state = watchdog::WatchdogState::new();
    watchdog::update(&mut state, |s| {
        s.all_files = file_list.len();
        s.all_pieces = (whole_size / piece_size) as usize;
        s.piece_size = piece_size as usize;
    });

    let want_progress = if m.mt {
        multithread::want_progress()
    } else {
        singlethread::want_progress()
    };

    let w = if want_progress {
        Some(watchdog::spawn(state.clone()))
    } else {
        None
    };

    let hash_list = if m.mt {
        multithread::calc_pieces(&file_list, piece_size, whole_size, &mut state)?
    } else {
        singlethread::calc_pieces(&file_list, piece_size, &mut state)?
    };

    watchdog::update(&mut state, |s| {
        s.finish = true;
    });

    let _ = w.map(|w| w.join());

    info!("Calculated {} pieces.", hash_list.len());

    let size_info = SizeInfo { piece_size, whole_size };
    let file_lists = FileLists { file_list, hash_list };

    create_torrent(&m, size_info, file_lists)?;

    Ok(())
}

trait AllowStrRef<T> {
    fn insert_str(&mut self, s: &str, t: T) -> Option<T>;
}

impl <T> AllowStrRef<T> for HashMap<String, T> {
    fn insert_str(&mut self, s: &str, t: T) -> Option<T> {
        self.insert(s.to_string(), t)
    }
}

fn remove_output_if_needed(m: &CreateModeArgs) -> Result<(), AppError> {
    if let Ok(meta) = fs::metadata(&m.out_name) {
        if m.force && meta.is_file() {
            fs::remove_file(&m.out_name)?;
        }
    }

    Ok(())
}

fn parent_dir_of(path_str: &String) -> String {
    let path = PathBuf::from(&path_str);
    match path.components().next() {
        Some(comp) => {
            let comp_path: &Path = comp.as_ref();
            return comp_path.to_str().unwrap().to_string();
        },
        None =>
            path.to_str().unwrap().to_string()
    }
}

fn strip_base_dir(full_path: impl AsRef<str>, base_dir: impl AsRef<str>) -> String {
    let el = full_path.as_ref();

    if el.starts_with(base_dir.as_ref()) {
        let (_, right) = el.split_at(base_dir.as_ref().len());
        right.trim_start_matches("/").to_string()
    } else {
        el.to_string()
    }
}

fn serialize_to_torrent(out_name: &String, root: TorrentSection) -> AppResult<()> {
    let mut fw = OpenOptions::new().write(true).create(true).open(out_name)?;

    BencodeElem::Dictionary(root).write_into(&mut fw)?;

    Ok(())
}


fn create_root_section(
    m: &CreateModeArgs,
    info: TorrentSection) -> AppResult<TorrentSection>
{
    let mut root = TorrentSection::new();
    let version_str = format!("{} v{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));

    root.insert_str("announce", BencodeElem::String(m.announce.clone()));
    root.insert_str("info", BencodeElem::Dictionary(info));

    if !m.anonymous {
        root.insert_str("created by", BencodeElem::String(version_str));
    }

    if let Some(ref comment) = m.comment {
        root.insert_str("comment", BencodeElem::String(comment.clone()));
    }

    Ok(root)
}

fn create_info_section(
    m: &CreateModeArgs,
    base_dir: &String,
    size_info: &SizeInfo,
    file_lists: &FileLists) -> AppResult<TorrentSection>
{
    let mut info = TorrentSection::new();

    let first_path_no_basedir = strip_base_dir(&file_lists.file_list[0], base_dir);

    if m.private {
        info.insert_str("private", BencodeElem::Integer(1));
    }

    if file_lists.file_list.len() == 1 {
        info.insert_str("length", BencodeElem::Integer(size_info.whole_size as i64));
        info.insert_str("name", BencodeElem::String(first_path_no_basedir));
    } else {
        let files = file_lists.file_list
            .iter()
            .map(|el| {
                strip_base_dir(el, base_dir)
            }).map(|path| {
                let mut full_path = PathBuf::from(&base_dir);
                full_path.push(&path);
                let file_size = fs::metadata(&full_path).unwrap().len();
                (path, file_size as i64)
            }).map(|(path, file_size)| {
                let mut entry_map = HashMap::<String, BencodeElem>::new();
                entry_map.insert_str("length", BencodeElem::Integer(file_size));
                entry_map.insert_str("path", BencodeElem::List(
                    vec![BencodeElem::String(path)]
                ));

                BencodeElem::Dictionary(entry_map)
            })
            .collect();

        info.insert_str("files", BencodeElem::List(files));

        let first_dir = parent_dir_of(&first_path_no_basedir);
        info.insert_str("name", BencodeElem::String(first_dir.clone()));
    }

    let mut growable_buf: Vec<u8> = Vec::new();

    file_lists.hash_list.iter().map(|el| {
        hex::decode(el).unwrap()
    }).for_each(|chunk| {
        for b in chunk {
            growable_buf.push(b);
        }
    });

    info.insert_str("pieces", BencodeElem::Bytes(growable_buf));
    info.insert_str("piece length", BencodeElem::Integer(size_info.piece_size as i64));

    Ok(info)
}

fn create_torrent(m: &CreateModeArgs, size_info: SizeInfo, file_lists: FileLists)
    -> AppResult<()>
{
    trace!("B-encoding torrent...");

    let base_dir = match m.base_dir {
        Some(ref dir) => dir.clone(),
        None => std::env::current_dir().unwrap().to_str().unwrap().to_string(),
    };

    let info = create_info_section(m, &base_dir, &size_info, &file_lists)?;
    let root = create_root_section(m, info)?;

    serialize_to_torrent(&m.out_name, root)
}

