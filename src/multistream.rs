use std::io::{Result, Read, ErrorKind, Error};
use std::fs::File;

pub struct MultiStream {
    file_list: Vec<String>,
    cur_index: Option<usize>,
    reader: Option<Box<dyn Read + Send>>,
    bytes_left: u64,
}

impl MultiStream {
    pub fn new(file_list: &Vec<String>) -> MultiStream {
        MultiStream {
            file_list: file_list.clone(),
            cur_index: None,
            reader: None,
            bytes_left: 0u64,
        }
    }

    pub fn cur_index(&self) -> Option<usize> { self.cur_index }

    fn advance_file(&mut self) -> Result<bool> {
        if self.file_list.is_empty() {
            return Ok(false);
        }

        if self.cur_index.is_none() {
            self.cur_index = Some(0);
        } else {
            self.cur_index = Some(self.cur_index.unwrap() + 1);
        }

        if self.cur_index.unwrap() >= self.file_list.len() {
            return Ok(false);
        } else {
            let filename = &self.file_list[self.cur_index.unwrap()];
            let f = match File::open(filename) {
                Ok(f) => f,
                Err(err) => {
                    error!("I/O error when trying to open {}", filename);
                    error!("Can't continue, aborting.");

                    let err_msg = format!("I/O error when trying to open {}: \
                        {}", filename, err);

                    return Err(Error::new(err.kind(), err_msg));
                }
            };

            self.bytes_left = f.metadata()?.len();
            self.reader = Some(Box::new(f));

            return Ok(true);
        }
    }
}

impl Read for MultiStream {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        if self.cur_index.is_none() || self.bytes_left == 0 {
            if self.advance_file()? == false {
                return Err(Error::new(ErrorKind::Other,
                    "tried to advance past the file list"));
            }
        }

        let buf_size = buf.len();
        let mut slice_ptr = 0;

        loop {
            let slice = &mut buf[slice_ptr .. buf_size];

            let bytes_read: usize = self.reader.as_mut().unwrap().read(slice)?;
            slice_ptr += bytes_read;
            if slice_ptr >= buf_size {
                return Ok(slice_ptr);
            }

            if bytes_read == 0 {
                if self.advance_file()? == false {
                    return Ok(slice_ptr);
                }
            }
        }
    }
}

