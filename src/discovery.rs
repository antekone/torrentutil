use std::path::Path;
use walkdir::WalkDir;

use crate::AppResult;
use std::path::{PathBuf};
use regex::Regex;

trait ListFiles {
    type IterState;

    fn reset(&self, iter_state: &mut Self::IterState);
    fn next_file(&self, iter_state: &mut Self::IterState) -> Option<&PathBuf>;
}

struct WalkerIterator<'a, A: ListFiles> {
    inner: &'a A,
    iter_state: A::IterState,
}

impl <'a, A: ListFiles> Iterator for WalkerIterator<'a, A> {
    type Item = &'a PathBuf;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next_file(&mut self.iter_state)
    }
}

struct SimulatedWalker {
    array: SimulatedWalkerArray,
}

struct SimulatedIterState {
    idx: usize,
}

impl Default for SimulatedIterState {
    fn default() -> Self {
        Self { idx: 0 }
    }
}

impl <'a> SimulatedWalker {
    fn new(source: impl Into<SimulatedWalkerArray>) -> Self {
        Self { array: source.into() }
    }

    fn iter(&mut self) -> WalkerIterator<SimulatedWalker> {
        WalkerIterator {
            inner: self,
            iter_state: SimulatedIterState::default()
        }
    }

    fn dummy(&mut self) { }
}

impl ListFiles for SimulatedWalker {
    type IterState = SimulatedIterState;

    fn reset(&self, state: &mut SimulatedIterState) {
        state.idx = 0;
    }

    fn next_file(&self, state: &mut SimulatedIterState) -> Option<&PathBuf> {
        if state.idx >= self.array.inner.len() {
            None
        } else {
            let r = &self.array.inner[state.idx];
            state.idx += 1;
            Some(r)
        }
    }
}

struct SimulatedWalkerArray {
    inner: Vec<PathBuf>
}

impl From<Vec<String>> for SimulatedWalkerArray {
    fn from(v: Vec<String>) -> Self {
        Self { inner: v.iter().map(|s| s.into()).collect() }
    }
}

impl From<Vec<&str>> for SimulatedWalkerArray {
    fn from(v: Vec<&str>) -> Self {
        Self { inner: v.iter().map(|s| s.into()).collect() }
    }
}

struct RegexMatchIterator<'a, W: ListFiles> {
    regex_list: Vec<Regex>,
    expected_match_result: bool,
    iter: &'a mut WalkerIterator<'a, W>,
}

impl <'a, W: ListFiles> RegexMatchIterator<'a, W> {
    fn new(
        iter: &'a mut WalkerIterator<'a, W>,
        regex_list: Vec<Regex>,
        expected_match_result: bool
    ) -> Self {
        Self { regex_list, iter, expected_match_result }
    }

    // Convenience constructors: for include-style iterator, and exclude-style
    // iterator, respectively.

    fn new_include(
        iter: &'a mut WalkerIterator<'a, W>,
        regex_list: Vec<Regex>
    ) -> Self {
        RegexMatchIterator::new(iter, regex_list, true)
    }

    fn new_exclude(
        iter: &'a mut WalkerIterator<'a, W>,
        regex_list: Vec<Regex>
    ) -> Self {
        RegexMatchIterator::new(iter, regex_list, false)
    }

    // Helper method that iterates over a stored list of regexps and returns
    // true, if a match with the list will be found.

    fn regex_list_matches(&mut self, path: &PathBuf) -> bool {
        self.regex_list.iter().any(|r| {
            let s = path.to_str().unwrap();
            r.is_match(s)
        })
    }
}

impl <'a, W: ListFiles> Iterator for RegexMatchIterator<'a, W> {
    type Item = &'a PathBuf;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.iter.next() {
                Some(item) => {
                    let match_result = self.regex_list_matches(item);
                    if self.expected_match_result == match_result {
                        return Some(item)
                    }
                },
                None =>
                    return None
            }
        }
    }
}

fn filter_excludes(de: &walkdir::DirEntry, excludes: &Vec<Regex>) -> bool {
    let s = de.path().as_os_str().to_str().unwrap();
    let excluded = excludes.iter().find(|&r| {
        r.is_match(s)
    }).is_some();

    !excluded
}

pub fn discover_files_with_lister<T>(
    lister: &mut dyn ListFiles<IterState=T>,
) {
    // todo ;p
}

pub fn discover_files(
    path: &String,
    excludes: &Vec<Regex>,
    out: &mut Vec<String>) -> AppResult<u64>
{
    trace!("Discovering files in arg '{}'...", path);

    let mut whole_size = 0u64;
    let walker = WalkDir::new(path);
    for entry in walker
        .into_iter()
        .filter_entry(|e| filter_excludes(e, excludes))
    {
        let path = entry?.path().to_str().unwrap_or("").to_string();
        if path.len() > 0 {
            let m = match std::fs::metadata(&path) {
                Ok(m) => m,
                Err(msg) => {
                    warn!("Skipping file because of an I/O error when trying to access {}: {}", path, msg);
                    continue;
                }
            };

            if m.is_file() {
                match std::fs::OpenOptions::new().read(true).open(&path) {
                    Ok(_) => out.push(path),
                    Err(msg) => {
                        warn!("Skipping file because of an I/O error when trying to open {}: {}", path, msg);
                        continue;
                    }
                };
            }


            whole_size += m.len();
        }
    }

    Ok(whole_size)
}

#[test]
fn test_simulated_walker_works_as_expected() {
    let mut walker = SimulatedWalker::new(vec!["a", "b", "c"]);
    let mut out = Vec::<String>::new();

    for entry in walker.iter() {
        out.push(entry.to_str().unwrap().to_string());
    }

    assert_eq!(out.len(), 3);
    assert_eq!(out[0], "a");
    assert_eq!(out[1], "b");
    assert_eq!(out[2], "c");
}

#[test]
fn test_inclusion_filtering_works_as_expected_case_1() {
    let mut walker = SimulatedWalker::new(vec!["a", "b", "c"]);
    let mut out = Vec::<String>::new();

    let list = vec!["^b$"].iter().map(|s| Regex::new(s).unwrap()).collect();
    let mut iter = walker.iter();
    let mut filter = RegexMatchIterator::new_include(&mut iter, list);

    for entry in filter {
        out.push(entry.to_str().unwrap().to_string());
    }

    assert_eq!(out.len(), 1);
    assert_eq!(out[0], "b");
}

#[test]
fn test_inclusion_filtering_works_as_expected_case_2() {
    let mut walker = SimulatedWalker::new(vec!["a", "b", "c"]);
    let mut out = Vec::<String>::new();

    let list = vec!["^[ab]$"].iter().map(|s| Regex::new(s).unwrap()).collect();
    let mut iter = walker.iter();
    let mut filter = RegexMatchIterator::new_include(&mut iter, list);

    for entry in filter {
        out.push(entry.to_str().unwrap().to_string());
    }

    assert_eq!(out.len(), 2);
    assert_eq!(out[0], "a");
    assert_eq!(out[1], "b");
}

#[test]
fn test_exclusion_filtering_works_as_expected_case_1() {
    let mut walker = SimulatedWalker::new(vec!["a", "b", "c"]);
    let mut out = Vec::<String>::new();


    let list = vec!["^[ab]$"].iter().map(|s| Regex::new(s).unwrap()).collect();
    let mut iter = walker.iter();
    let mut filter = RegexMatchIterator::new_exclude(&mut iter, list);

    for entry in filter {
        out.push(entry.to_str().unwrap().to_string());
    }

    assert_eq!(out.len(), 1);
    assert_eq!(out[0], "c");
}