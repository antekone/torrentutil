use lava_torrent::torrent::v1::Torrent;
use lava_torrent::bencode::BencodeElem;
use num_format::{ToFormattedString};
use super::utils;
use chrono::prelude::*;

type AppResult = Result<(), super::AppError>;

pub fn run_inspect(args: super::InspectModeArgs) -> AppResult {
    let torrent = Torrent::read_from_file(&args.filename)?;

    if !args.list_fields && !args.list_files && !args.list_pieces {
        list_basic(&torrent, &args.filename, args.verbose);
        return Ok(());
    }

    if args.list_fields {
        list_fields(&torrent);
    }

    if args.list_files {
        list_files(&torrent, args.verbose);
    }

    if args.list_pieces {
        list_pieces(&torrent, args.verbose);
    }

    Ok(())
}

fn list_basic(torrent: &Torrent, filename: &String, _verbose: bool) {
    let locale = utils::default_locale();

    let announce_str = torrent.announce.clone().unwrap_or("<missing>".to_string());
    let size_str = torrent.length.to_formatted_string(&locale);
    let no_of_pieces = torrent.pieces.len().to_formatted_string(&locale);
    let piece_size = torrent.piece_length.to_formatted_string(&locale);
    let no_of_files = if let Some(ref f) = torrent.files { f.len() } else { 0 };
    let torrent_size = std::fs::metadata(filename).unwrap().len().to_formatted_string(&locale);

    let mut bep12_seen = false;

    println!("File:         {}", filename);
    println!("Torrent size: {} bytes", torrent_size);
    println!("Announce:     {}", announce_str);

    match torrent.announce_list {
        Some(ref lst) => {
            lst.iter().for_each(|v| {
                v.iter().for_each(|e| {
                    println!("BEP12:     {}", e);
                    if !bep12_seen {
                        bep12_seen = true
                    };
                });
            });
        },
        None => (),
    }

    println!("Size:         {} bytes", size_str);
    println!("Piece sz:     {} bytes", piece_size);
    println!("# of pcs:     {} items", no_of_pieces);

    if no_of_files == 0 {
        println!("# files:      1 file");
        println!("Base dir:     (none)");
    } else {
        println!("# files:      {} files", no_of_files.to_formatted_string(&locale));
        println!("Base dir:     '{}'", torrent.name);
    }

    if bep12_seen {
        println!("");
        println!("Note: Information about BEP12 can be found here: http://bittorrent.org/beps/bep_0012.html");
    }
}

fn list_pieces(t: &Torrent, _verbose: bool) {
    let max_length_size = t.pieces.len().to_string().len();
    let locale = utils::default_locale();

    println!("Piece size: {} bytes", t.piece_length.to_formatted_string(&locale));
    println!();

    println!("{:>width$} | hash", "idx", width = max_length_size);

    for idx in 0 .. t.pieces.len() {
        let p = &t.pieces[idx];
        let mut buf = String::new();
        for ch in p {
            buf.push_str(format!("{:02X}", ch).as_str());
        }

        println!("{:>width$} | {}", idx, buf, width = max_length_size);
    }
}

fn list_files(t: &Torrent, verbose: bool) {
    let empty = Vec::new();
    let files = t.files.as_ref().unwrap_or(&empty);

    let mut max_length_size = files.iter()
        .map(|f| { f.length.to_string().len() })
        .max()
        .unwrap_or(t.length.to_string().len());

    max_length_size += max_length_size / 3;

    let locale = utils::default_locale();
    if verbose {
        println!("Using locale: {}", locale.name());
    }

    println!("{:>width$} |  idx  | name", "size", width = max_length_size);

    let mut idx = 0;
    files.iter().for_each(|f| {
        // TODO: list extra fields of each file (if exists)
        let path = f.path.to_str().unwrap_or("<n/a>");
        let length_fmt = f.length.to_formatted_string(&locale);
        println!("{:>width$} | {:5} | {}", length_fmt, idx, path, width = max_length_size);
        idx += 1;
    });

    if idx == 0 {
        let length_fmt = t.length.to_formatted_string(&locale);
        println!("{:>width$} | {:5} | {}", length_fmt, 0, t.name, width = max_length_size);
    }
}

fn list_fields(t: &Torrent) {
    if let Some(ref d) = t.extra_fields {
        d.iter().for_each(|e| {
            let (key, value) = e;
            print!("Extra field: {} = {:?}", key, value);

            match if key == "creation date" {
                Some(match value {
                    BencodeElem::Integer(timestamp) => {
                        let naive = NaiveDateTime::from_timestamp(*timestamp, 0);
                        let datetime: DateTime<Utc> = DateTime::from_utc(naive, Utc);

                        format!("UTC: {}", datetime.format("%Y-%m-%d %H:%M:%S"))
                    },
                    _ => format!("invalid value!"),
                })
            } else { None } {
                Some(data) => println!(" -- {}", data),
                None => println!("")
            }
        });
    }

    if let Some(ref d) = t.extra_info_fields {
        d.iter().for_each(|e| {
            let (key, value) = e;

            print!("Extra info field: {} = {:?}", key, value);

            match if key == "private" {
                Some(match value {
                    BencodeElem::Integer(1) => format!("private, PEX/DHT disabled"),
                    BencodeElem::Integer(0) => format!("public, PEX/DHT enabled"),
                    _ => format!("invalid value!"),
                })
            } else {
                None
            } {
                Some(data) => println!(" -- {}", data),
                None => println!("")
            }
        });
    }
}
