use super::{AppError, watchdog};
use super::watchdog::Watchdog;
use super::multistream::MultiStream;
use crate::openssl::sha::Sha1;
use std::io::Read;
use crate::safe_uninit::ResizeUninit;

pub fn want_progress() -> bool { true }

pub fn calc_pieces(file_list: &Vec<String>, piece_size: u64, state: &mut Watchdog) -> Result<Vec<String>, AppError> {
    let mut stream = MultiStream::new(file_list);
    let mut buf = Vec::<u8>::new();
    buf.resize_uninit(piece_size as usize);

    let mut idx = 0;
    let mut list = Vec::new();

    loop {
        let ret = stream.read(&mut *buf)?;
        if ret == 0 { break };

        let mut hash = Sha1::new();
        hash.update(&(*buf)[0..ret]);
        let result = hash.finish();

        let hash_str = hex::encode(result);
        list.push(hash_str);

        idx += 1;

        watchdog::update(state, |dog| {
            dog.cur_file = stream.cur_index().unwrap_or(0);
            dog.cur_piece = idx;
        });
    }

    watchdog::update(state, |dog| {
        dog.finish = true;
    });

    Ok(list)
}

