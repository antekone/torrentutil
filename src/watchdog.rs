use std::sync::Arc;
use std::time::Duration;
use std::thread;
use crate::indicatif::{ProgressBar, ProgressStyle};
use crate::parking_lot::RwLock;

pub struct WatchdogState {
    pub cur_file: usize,
    pub all_files: usize,
    pub cur_piece: usize,
    pub all_pieces: usize,
    pub piece_size: usize,
    pub finish: bool,
}

pub type Watchdog = Arc<RwLock<WatchdogState>>;

impl WatchdogState {
    pub fn new() -> Watchdog {
        Arc::new(RwLock::new(WatchdogState {
            cur_file: 0,
            all_files: 0,
            finish: false,
            all_pieces: 0,
            cur_piece: 0,
            piece_size: 0,
        }))
    }
}

pub fn update<F>(dog: &mut Watchdog, mut f: F)
where
    F: FnMut(&mut WatchdogState) -> ()
{
    let mut state = dog.write();
    f(&mut *state);
}

pub fn read<F>(dog: &Watchdog, mut f: F)
where
    F: FnMut(&WatchdogState) -> ()
{
    let state = dog.read();
    f(&*state);
}

pub fn spawn(watchdog: Watchdog) -> std::thread::JoinHandle<()> {
    thread::spawn(move || {
        let mut finished = false;
        let mut cur_file = 0;
        let mut all_files = 0;
        let mut all_pieces = 0;
        let mut cur_piece = 0;
        let mut piece_size = 0;

        read(&watchdog, |d| {
            all_files = d.all_files;
            all_pieces = d.all_pieces;
            piece_size = d.piece_size;
        });

        let pb = ProgressBar::new(all_pieces as u64 * piece_size as u64);
        pb.set_style(ProgressStyle::default_bar().template("{spinner} {bytes}/{total_bytes} {msg} [{percent}%] ETA: {eta}"));

        loop {
            read(&watchdog, |d| {
                finished = d.finish;
                cur_file = d.cur_file;
                cur_piece = d.cur_piece;
            });

            if finished { break };

            pb.set_position(cur_piece as u64 * piece_size as u64);
            pb.set_message(format!("P: {}/{} F: {}/{}", cur_piece, all_pieces, cur_file, all_files).as_str());
            thread::sleep(Duration::from_millis(250));
        }

        pb.finish_with_message("finished");
    })
}

