#![allow(unused_variables)]
#![allow(unused_assignments)]
#![allow(unused_mut)]
#![allow(unreachable_code)]
#![allow(dead_code)]

use std::io::Read;
use std::thread;
use std::sync::{Arc, Barrier};
use std::cmp;
use super::{AppError, watchdog};
use super::watchdog::Watchdog;
use super::multistream::MultiStream;
use crate::num_cpus;
use crate::parking_lot::{RwLock, Mutex};
use crate::safe_uninit::ResizeUninit;
use crate::openssl::sha::Sha1;
use crate::rayon::prelude::*;

#[derive(Debug)]
struct Range {
    begin: usize,
    end: usize
}

impl std::fmt::Display for Range {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Range[0x{:08x}-0x{:08x}]", self.begin, self.end)
    }
}

pub fn want_progress() -> bool { true }

pub fn calc_pieces(file_list: &Vec<String>, piece_size: u64, total_bytes: u64, state: &mut Watchdog) -> Result<Vec<String>, AppError> {
    let method = 1;

    match method {
        0 => calc_pieces_par_iter(file_list, piece_size, total_bytes, state),
        1 => calc_pieces_manual(file_list, piece_size, total_bytes, state),
        _ => Err(AppError::Unimplemented)
    }
}

enum LightweightIOError {
    OK, Error(String)
}

struct HasherState {
    barrier: Arc<Barrier>,
    barrier2: Arc<Barrier>,
    finished_flag: Arc<Mutex<bool>>,
    range: Range,
    last_read: Arc<RwLock<usize>>,
    buf_access: Arc<RwLock<Vec<u8>>>,
    hash_list: Arc<RwLock<Vec<[u8; 20]>>>,
    debug_index: usize,
}

struct IOState {
    stream: MultiStream,
    buf_access: Arc<RwLock<Vec<u8>>>,
    finished_flag: Arc<Mutex<bool>>,
    io_error: Arc<RwLock<LightweightIOError>>,
    barrier: Arc<Barrier>,
    barrier2: Arc<Barrier>,
    last_read: Arc<RwLock<usize>>,
    watchdog_state: Option<Watchdog>,
    worker_count: usize,
}

struct Worker {
    handle: Option<thread::JoinHandle<()>>,
    hash_list: Arc<RwLock<Vec<[u8; 20]>>>,
}

fn hasher_thread(state: HasherState) {
    let output = &mut *state.hash_list.write();
    loop {
        state.barrier.wait(); // Wait for data from I/O thread.

        if *state.finished_flag.lock() {
            return;
        }

        // Perform hashing of data.

        let bytes_read = { *state.last_read.read() };

        if bytes_read > state.range.begin {
            let range_end = cmp::min(state.range.end, bytes_read);
            let buf = &*state.buf_access.read();

            let mut hash = Sha1::new();
            hash.update(&buf[state.range.begin .. range_end + 1]);
            let result = hash.finish();

            output.push(result);
        }

        state.barrier2.wait(); // Signal hashing done.
    }
}

fn io_thread(mut state: IOState) {
    let mut piece_idx = 0;
    let mut watchdog_state = state.watchdog_state.take().unwrap();
    loop {
        {
            let locked_buf = &*state.buf_access;
            let mut buf = locked_buf.write();

            let bytes_read = match state.stream.read(&mut buf) {
                Ok(0) => {
                    // No more files to process, finish thread.
                    *state.finished_flag.lock() = true;
                    return;
                },
                Ok(n) => n,
                Err(err) => {
                    *state.io_error.write() = LightweightIOError::Error(format!("{}", err));
                    return;
                }
            };

            *state.last_read.write() = bytes_read - 1;

            // Unlock buf on scope exit.
        }

        state.barrier.wait(); // Signal hasher threads to start processing.
        state.barrier2.wait(); // Wait for hasher threads to finish processing.

        piece_idx += state.worker_count;

        watchdog::update(&mut watchdog_state, |dog| {
            dog.cur_file = state.stream.cur_index().unwrap_or(0);
            dog.cur_piece = piece_idx;
        });
    }

    watchdog::update(&mut watchdog_state, |dog| {
        dog.finish = true;
    });
}

pub fn calc_pieces_manual(file_list: &Vec<String>, piece_size: u64, _total_bytes: u64, state: &mut Watchdog) -> Result<Vec<String>, AppError> {
    let job_count = num_cpus::get();
    if job_count <= 1 {
        error!("Sorry, this system doesn't let me use multiple cores to perform faster hashing.");
        return Err(AppError::MultithreadingNotSupported);
    }

    let worker_count = job_count - 1;

    let mut buf = Vec::<u8>::new();
    buf.resize_uninit(worker_count * piece_size as usize);

    let mut workers = Vec::<Worker>::new();

    let io_error = Arc::new(RwLock::new(LightweightIOError::OK));
    let last_read = Arc::new(RwLock::new(0));
    let finished_flag = Arc::new(Mutex::new(false));
    let barrier = Arc::new(Barrier::new(worker_count + 1));
    let barrier2 = Arc::new(Barrier::new(worker_count + 1));

    let buf_access = Arc::new(RwLock::new(buf));

    for i in 0..worker_count {
        let range_beg = i * piece_size as usize;
        let range_end = range_beg + piece_size as usize - 1;

        let local_hash_list = Arc::new(RwLock::new(Vec::<[u8; 20]>::new()));

        let hasher_state = HasherState {
            barrier: barrier.clone(),
            barrier2: barrier2.clone(),
            finished_flag: finished_flag.clone(),
            range: Range { begin: range_beg, end: range_end },
            last_read: last_read.clone(),
            buf_access: buf_access.clone(),
            hash_list: local_hash_list.clone(),
            debug_index: i
        };

        let handle = thread::spawn(move || { hasher_thread(hasher_state) });

        workers.push(Worker {
            handle: Some(handle),
            hash_list: local_hash_list.clone(),
        });
    };

    let mut stream = MultiStream::new(file_list);

    let io_state = IOState {
        stream,
        buf_access,
        finished_flag: finished_flag.clone(),
        io_error: io_error.clone(),
        barrier: barrier.clone(),
        barrier2: barrier2.clone(),
        last_read: last_read.clone(),
        watchdog_state: Some(state.clone()),
        worker_count,
    };

    let fill_thread = thread::spawn(move || { io_thread(io_state) });

    let _ = fill_thread.join();

    match &*io_error.read() {
        LightweightIOError::OK => {
        },

        LightweightIOError::Error(msg) => {
            println!("Error in I/O thread: {}", msg);
        }
    }

    *finished_flag.lock() = true;
    barrier.wait();

    for w in &mut workers {
        let _ = w.handle.take().map(thread::JoinHandle::join);
    }

    let mut idx = 0;
    let mut sv = Vec::<String>::new();

    let mut w = 0;
    let mut i = 0;

    loop {
        let worker = &workers[w];
        let vec = &*worker.hash_list.read();
        if i >= vec.len() { break; }

        let hash = &vec[i];
        sv.push(hex::encode(hash));

        w += 1;
        if w >= workers.len() {
            w = 0;
            i += 1;
        }
    }

    Ok(sv)
}

pub fn calc_pieces_par_iter(file_list: &Vec<String>, piece_size: u64, total_bytes: u64, state: &mut Watchdog) -> Result<Vec<String>, AppError> {
    let job_count = num_cpus::get();
    if job_count <= 1 {
        error!("Sorry, this system doesn't let me use multiple cores to perform faster hashing.");
        return Err(AppError::MultithreadingNotSupported);
    }

    let mut buf = Vec::<u8>::new();
    let mut ranges = Vec::<Range>::new();
    buf.resize_uninit(job_count * piece_size as usize);

    for i in 0..job_count {
        let range_beg = i * piece_size as usize;
        let range_end = range_beg + piece_size as usize - 1;
        ranges.push(Range { begin: range_beg, end: range_end });
    }

    let mut stream = MultiStream::new(file_list);
    let mut hashes = Vec::<String>::new();
    let mut full_hashes = Vec::<String>::new();
    let full_chunks = total_bytes / (job_count as u64 * piece_size);

    hashes.reserve(job_count);

    let mut idx = 0;

    for _ in 0..full_chunks {
        stream.read_exact(&mut *buf)?;

        hashes.clear();
        hashes = ranges.par_iter().map(|s| {
            let mut hash = Sha1::new();
            hash.update(&(*buf)[s.begin .. s.end + 1]);
            let result = hash.finish();
            hex::encode(result)
        }).collect::<Vec::<String>>();

        idx += job_count;

        watchdog::update(state, |dog| {
            dog.cur_file = stream.cur_index().unwrap_or(0);
            dog.cur_piece = idx;
        });

        full_hashes.append(&mut hashes);
    }

    watchdog::update(state, |dog| {
        dog.finish = true;
    });

    Ok(full_hashes)
}
