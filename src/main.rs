use clap::{App, Arg};
use std::io;
use std::result::Result;
use regex::Regex;

mod inspect;
mod utils;
mod create;
mod watchdog;
mod singlethread;
mod multithread;
mod multistream;
mod discovery;

extern crate num_format;
extern crate lava_torrent;
extern crate thiserror;
#[macro_use] extern crate log;
extern crate simple_logger;
extern crate chrono;
extern crate walkdir;
extern crate hexdump;
//extern crate crypto;
extern crate openssl;
extern crate hex;
extern crate indicatif;
extern crate parking_lot;
extern crate num_cpus;
extern crate safe_uninit;
extern crate rayon;
extern crate regex;

// Inspect option names
const ARG_VERBOSE: &'static str = "verbose";
const ARG_LIST: &'static str = "list";
const ARG_FIELDS: &'static str = "fields";
const ARG_PIECES: &'static str = "pieces";
const ARG_FILENAME: &'static str = "filename";

// Create option names
const ARG_PRIVATE: &'static str = "private";
const ARG_ANNOUNCE: &'static str = "announce";
const ARG_EXCLUDE: &'static str = "exclude";
const ARG_PIECESIZE: &'static str = "piecesize";
const ARG_PATH: &'static str = "path";
const ARG_MULTITHREADING: &'static str = "mt";
const ARG_OUT: &'static str = "out_name";
const ARG_FORCE: &'static str = "force";
const ARG_BASE_DIR: &'static str = "base_dir";
const ARG_COMMENT: &'static str = "comment";
const ARG_ANONYMOUS: &'static str = "anonymous";

// Main mode names
const ARG_INSPECT: &'static str = "inspect";
const ARG_CREATE: &'static str = "create";

type AppResult<T> = std::result::Result<T, AppError>;

#[derive(thiserror::Error, Debug)]
pub enum AppError {
    #[error("I/O error")]
    IOError(#[from] io::Error),

    #[error("Torrent I/O error")]
    TorrentError(#[from] lava_torrent::error::Error),

    #[error("I/O error during file discovery")]
    WalkdirError(#[from] walkdir::Error),

    #[error("File not found")]
    FileNotFoundError(String),

    #[error("Name is busy")]
    NameIsBusy(String),

    #[error("Missing/wrong arguments")]
    WrongArguments,

    #[error("Missing announce URL")]
    MissingAnnounce,

    #[error("Missing files")]
    MissingFiles,

    #[error("Unimplemented")]
    Unimplemented,

    #[error("Multithreading not supported")]
    MultithreadingNotSupported,
}

pub struct InspectModeArgs {
    filename: String,
    verbose: bool,
    list_files: bool,
    list_fields: bool,
    list_pieces: bool,
}

pub struct CreateModeArgs {
    private: bool,
    verbose: bool,
    anonymous: bool,
    announce: String,
    paths: Vec<String>,
    excludes: Vec<Regex>,
    piece_size: Option<u64>,
    mt: bool,
    out_name: String,
    force: bool,
    base_dir: Option<String>,
    comment: Option<String>,
}

fn set_verbose_log(verbose: bool) {
    if verbose {
        log::set_max_level(log::LevelFilter::Trace);
    } else {
        log::set_max_level(log::LevelFilter::Warn);
    }
}

fn parse_inspect_args(m: &clap::ArgMatches) -> Result<(), AppError> {
    let args = InspectModeArgs {
        filename: m.value_of(ARG_FILENAME).ok_or(AppError::WrongArguments)?.to_string(),
        verbose: m.is_present(ARG_VERBOSE),
        list_files: m.is_present(ARG_LIST),
        list_fields: m.is_present(ARG_FIELDS),
        list_pieces: m.is_present(ARG_PIECES),
    };

    set_verbose_log(args.verbose);
    inspect::run_inspect(args)
}

fn parse_piece_size(s: &str) -> Option<u64> {
    let ls = s.to_lowercase();
    let base = if ls.starts_with("0x") { 16 } else { 10 };
    let mul = if ls.ends_with("k") || ls.ends_with("kb") {
        1024
    } else if ls.ends_with("m") || ls.ends_with("mb") {
        1024 * 1024
    } else {
        1
    };

    let digits = s.chars().filter(|c| c.is_digit(base)).collect::<String>();
    match u64::from_str_radix(digits.as_str(), base) {
        Ok(r) => Some(r * mul),
        Err(conv_err) => {
            warn!("Invalid number passed as 'piece size' argument -- ignoring option.");
            warn!("Conversion error message: {}", conv_err);
            return None;
        }
    }
}

fn parse_create_args(m: &clap::ArgMatches) -> AppResult<()> {
    let excludes = match m.values_of(ARG_EXCLUDE) {
        Some(items) => items.map(|e| e.to_string()).collect::<Vec::<String>>(),
        None => Vec::new(),
    };

    let exclude_regexps = excludes
        .iter()
        .map(|s| Regex::new(s))
        .collect::<Vec::<Result<Regex, regex::Error>>>();

    for regexp in exclude_regexps.iter() {
        if regexp.is_err() {
            error!("Invalid regular expression provided: {}", regexp.as_ref().err().unwrap());
            return AppResult::Err(AppError::WrongArguments);
        }
    }

    let paths = match m.values_of(ARG_PATH) {
        Some(items) => items.map(|e| e.to_string()).collect::<Vec::<String>>(),
        None => Vec::new(),
    };

    let args = CreateModeArgs {
        paths,
        excludes: exclude_regexps.iter().map(|r| r.clone().unwrap()).collect::<Vec<Regex>>(),
        private: m.is_present(ARG_PRIVATE),
        verbose: m.is_present(ARG_VERBOSE),
        anonymous: m.is_present(ARG_ANONYMOUS),
        announce: m.value_of(ARG_ANNOUNCE).ok_or(AppError::MissingAnnounce)?.to_string(),
        piece_size: m.value_of(ARG_PIECESIZE).and_then(|s| { parse_piece_size(s) }),
        mt: m.is_present(ARG_MULTITHREADING),
        out_name: m.value_of(ARG_OUT).unwrap_or("out.torrent").to_string(),
        force: m.is_present(ARG_FORCE),
        base_dir: m.value_of(ARG_BASE_DIR).map(str::to_string),
        comment: m.value_of(ARG_COMMENT).map(str::to_string),

        // TODO: introduce a thread limit option. Don't put a limit on it, but print a warning
        // that lots of threads do not necessarity mean there will be faster performance, because
        // of disk reading bottleneck.
        //
        // Alternative solution would be to provide some kind of a monitor inside
        // hash processing function that will display a warning if processing threads are
        // being starved, because of slow disk read speed.
    };

    set_verbose_log(args.verbose);
    create::run_create(args)
}

fn banner() {
    println!("{} {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
    println!("(c) 2020 by Grzegorz Antoniak");
    println!("");
}

fn run(m: clap::ArgMatches) -> i32 {
    banner();
    let ret = match m.subcommand() {
        (ARG_INSPECT, Some(matches)) =>
            parse_inspect_args(matches),

        (ARG_CREATE, Some(matches)) =>
            parse_create_args(matches),

        _ => {
            println!("{}: Please use the `--help` option to get some help.", env!("CARGO_PKG_NAME"));
            return 1;
        }
    };

    match ret {
        Err(AppError::Unimplemented) => {
            error!("Error: this code path is not implemented");
            1
        },

        Err(AppError::WalkdirError(err)) => {
            error!("Error during file discovery: {}", err);
            1
        },

        Err(AppError::IOError(err)) => {
            error!("I/O error: {}", err);
            1
        },

        Err(AppError::NameIsBusy(_)) => {
            error!("Name is busy");
            1
        },

        Err(AppError::FileNotFoundError(filename)) => {
            error!("File not found: {}", filename);
            1
        },

        Err(AppError::WrongArguments) => {
            error!("Wrong arguments provided. Please consult the `--help` option.");
            1
        },

        Err(AppError::MissingAnnounce) => {
            error!("Missing Announce URL. This is a required parameter!");
            1
        },

        Err(AppError::MissingFiles) => {
            error!("No files found, can't continue.");
            1
        },

        Err(AppError::MultithreadingNotSupported) => {
            error!("Only one core is available for processing, can't continue.");
            1
        },

        Err(AppError::TorrentError(err)) => {
            error!("Error while parsing the torrent file: {}", err);
            1
        },

        Ok(()) => 0,
    }
}

fn main() {
    simple_logger::init().unwrap();

    let inspect_subcmd =
        App::new(ARG_INSPECT)
            .about("Dumps information about selected torrent file")

            .arg(Arg::with_name(ARG_VERBOSE)
                .short('v')
                .long("verbose")
                .about("Be verbose when printing messages"))

            .arg(Arg::with_name(ARG_LIST)
                .short('l')
                .long("list")
                .about("List files"))

            .arg(Arg::with_name(ARG_FIELDS)
                .short('f')
                .long("fields")
                .about("List property fields"))

            .arg(Arg::with_name(ARG_PIECES)
                .short('p')
                .long("pieces")
                .about("List hash pieces"))

            .arg(Arg::with_name(ARG_FILENAME)
                .about("Path to the file")
                .takes_value(true)
                .required(true));

    let create_subcmd =
        App::new(ARG_CREATE)
            .about("Creates a torrent file according to provided options")

            .arg(Arg::with_name(ARG_PRIVATE)
                .short('p')
                .long("private")
                .about("Set 'private' flag"))

            .arg(Arg::with_name(ARG_VERBOSE)
                .short('v')
                .long("verbose")
                .about("Be verbose when printing messages"))

            .arg(Arg::with_name(ARG_ANONYMOUS)
                .long("anon")
                .about("Don't include 'created by' field in the output torrent"))

            .arg(Arg::with_name(ARG_ANNOUNCE)
                .short('a')
                .long("announce")
                .about("Set the Announce URL")
                .takes_value(true))

            .arg(Arg::with_name(ARG_EXCLUDE)
                .short('x')
                .long("exclude")
                .about("Exclude this path from processing. Regexps should be written to \
                    match an absolute path.")
                .takes_value(true)
                .multiple(true))

            .arg(Arg::with_name(ARG_PIECESIZE)
                .short('s')
                .long("size")
                .about("Specify piece size (normally in bytes, but will use kilobytes with 'k' suffix, or megabytes with 'm' suffix, e.g. '1m' will use a 1-megabyte piece size). Normally the piece size is calculated automatically based on the size of the files you want to put inside the torrent file, and there's no need to use this option, unless you want to override defaults.")
                .takes_value(true))

            .arg(Arg::with_name(ARG_MULTITHREADING)
                .short('m')
                .long("multithreading")
                .about("Use multithreaded processing"))

            .arg(Arg::with_name(ARG_PATH)
                .about("Path to the directory that contains necessary files (it should be a \
                    filename, or a directory name. In case of a directory, all files from this \
                    directory will be recursively added into the torrent file, excluding files \
                    specified in --exclude option)")
                .required(true)
                .multiple(false))

            .arg(Arg::with_name(ARG_OUT)
                .short('o')
                .long("out")
                .about("Name of the output torrent file (will be created)")
                .takes_value(true))

            .arg(Arg::with_name(ARG_FORCE)
                .short('f')
                .long("force")
                .about("Overwrite the output file if it exists"))

            .arg(Arg::with_name(ARG_BASE_DIR)
                .short('b')
                .long("base-dir")
                .about("Use this base path")
                .required(false)
                .takes_value(true))

            .arg(Arg::with_name(ARG_COMMENT)
                .short('c')
                .long("comment")
                .about("Embed a comment (string)")
                .required(false)
                .takes_value(true));

    // TODO: skip I/O errors during file discovery phase instead of bailing out.
    // current i/o error strategy during file discovery phase is to skip file, but this
    // doesn't seem to make much sense. Fail the whole process if an i/o error is found,
    // but add an option to override the behavior so that it works like now.
    //

    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .about("Torrent file creation/inspection utility")
        .subcommand(inspect_subcmd)
        .subcommand(create_subcmd)
        .get_matches();

    std::process::exit(run(matches));
}
