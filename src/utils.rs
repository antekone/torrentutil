use num_format::SystemLocale;

pub fn default_locale() -> SystemLocale {
    SystemLocale::default()
        .unwrap_or(SystemLocale::from_name("C").unwrap())
}
